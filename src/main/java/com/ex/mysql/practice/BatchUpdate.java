package com.ex.mysql.practice;

import com.ex.HibernateUtil;
import com.ex.mysql.model.Authority;
import com.ex.postgres.model.Partner;
import com.ex.postgres.model.Site;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.time.Instant;
import java.util.List;
import java.util.Random;

public class BatchUpdate {
    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory("mysql");
        batchInsert(sessionFactory, 3);
    }

    public static void batchInsert(SessionFactory sessionFactory, int batchSize) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Random random = new Random();
        for (int i = 1; i <= 10; i++) {
            Authority authority = new Authority();
            authority.setName("Authority_" + i + Instant.now());
            session.save(authority);
            if (i % batchSize == 0) {
                session.flush();
                session.clear();
            }
        }
        tx.commit();
        session.close();

    }

    public static void batchUpdate(SessionFactory sessionFactory, int batchSize) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        List<Authority> authorities = session.createQuery("FROM Authority s", Authority.class).list();
        Random random = new Random();
        int i = 0;
        for (Authority authority : authorities) {
            authority.setName(authority.getName() + "_" + random.nextInt());
            session.merge(authority);
            if (i++ % batchSize == 0) {
                session.flush();
                session.clear();
            }
        }
        tx.commit();
        session.close();
    }
}
