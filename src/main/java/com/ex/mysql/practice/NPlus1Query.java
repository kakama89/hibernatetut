package com.ex.mysql.practice;

import com.ex.HibernateUtil;
import com.ex.mysql.model.Authority;
import com.ex.mysql.model.User;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;
import org.hibernate.graph.RootGraph;
import org.hibernate.jpa.QueryHints;

import java.util.List;

public class NPlus1Query {
    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory("mysql");
//        entityGraph(sessionFactory, 3L);
//        nPlus1Query(sessionFactory);
//        criteria(sessionFactory, 1L);
    }


    public static void nPlus1Query(SessionFactory sessionFactory) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        List<User> users = session.createQuery("From User u", User.class).list();
        for (User u : users) {
            System.out.println(u.getAuthorities());
        }
        tx.commit();
        session.close();
    }

    // exception occurs when get associate with target object when session is closed
    public static void lazyException(SessionFactory sessionFactory, Long id){
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        User user = session.get(User.class, id);
        System.out.print(user.getUsername());
        tx.commit();
        session.close();
        System.out.println(user.getAuthorities());
    }

    // resolve n + 1 query
    public static void initialize(SessionFactory sessionFactory, Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        User user = session.get(User.class, id);
        System.out.print(user.getUsername());
        Hibernate.initialize(user.getAuthorities());
        tx.commit();
        session.close();
        System.out.println(user.getAuthorities());
    }

    // resolve n + 1 query
    public static void eagerFetch(SessionFactory sessionFactory, Long id){
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        User user = session.createQuery("FROM User u JOIN FETCH u.authorities WHERE u.id =:id", User.class).setParameter("id", id).uniqueResult();

        System.out.print(user.getUsername());
        tx.commit();
        session.close();
        System.out.println(user.getAuthorities());
    }

    // resolve n + 1 query
    public static void entityGraph(SessionFactory sessionFactory, Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        RootGraph<?> entityGraph = session.createEntityGraph("user-entity-graph");
        User user = session.createQuery("FROM User u WHERE u.id =:id", User.class)
                .setParameter("id", id)
                .setHint("javax.persistence.fetchgraph", entityGraph)
//                .setHint("javax.persistence.loadgraph", entityGraph)
                .uniqueResult();

        System.out.print(user.getUsername());
        tx.commit();
        session.close();
        System.out.println(user.getAuthorities());
    }


    public static void criteria(SessionFactory sessionFactory, Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Criteria criteria = session.createCriteria(User.class);
        User user = (User) criteria.setFetchMode("authorities", FetchMode.JOIN)
                .add(Restrictions.eq("id", id)).uniqueResult();


        System.err.println(user.getUsername());
        tx.commit();
        session.close();
        System.out.println(user.getAuthorities());
    }
}
