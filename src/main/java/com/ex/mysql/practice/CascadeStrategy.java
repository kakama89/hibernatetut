package com.ex.mysql.practice;

import com.ex.HibernateUtil;
import com.ex.mysql.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class CascadeStrategy {
    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory("mysql");
        mergeCascade(sessionFactory, 3L);
//        removeCascade(sessionFactory, 1L);
//        refreshCascade(sessionFactory, 2L);

    }
    public static void mergeCascade(SessionFactory sessionFactory, Long id){
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        User user = session.get(User.class, id);
        tx.commit();
        session.close();
        user.getAuthorities();
    }

    public static void removeCascade(SessionFactory sessionFactory ,  Long id){
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        User user = session.get(User.class, id);
        session.remove(user);
        tx.commit();
        session.close();
    }

    public static void refreshCascade(SessionFactory sessionFactory, Long id){
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        User user = session.get(User.class, id);
        user.setUsername("toai_new1223");
        session.refresh(user);
        tx.commit();
        session.close();
    }
}
