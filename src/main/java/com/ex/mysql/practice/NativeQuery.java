package com.ex.mysql.practice;

import com.ex.HibernateUtil;
import com.ex.mysql.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class NativeQuery {
    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory("mysql");
        createNativeQuery2(sessionFactory);
//        createHQLQuery(sessionFactory);
    }

    public static void createHQLQuery(SessionFactory sf){
        Session session = sf.openSession();
        Transaction tx = session.beginTransaction();
        List users = session.createQuery("FROM User u").list();
        System.err.println(users);
        System.err.println("Load hql :");
        User u = session.get(User.class, 1L);
        tx.commit();
        session.close();
    }

    public static void createNativeQuery(SessionFactory sf){
        Session session = sf.openSession();
        Transaction tx = session.beginTransaction();
        List users = session.createNativeQuery("SELECT * FROM tbl_user", User.class).list();
        System.err.println(users);
        System.err.println("Load native :");
        User u = session.get(User.class, 1L);
        tx.commit();
        session.close();
    }

    public static void createNativeQuery2(SessionFactory sf){
        Session session = sf.openSession();
        Transaction tx = session.beginTransaction();
        List users = session.createNativeQuery("SELECT * FROM tbl_user where id > 0 limit 3", User.class).list();
        System.err.println(users);
        System.err.println("Load native :");
        User u = session.get(User.class, 1L);
        tx.commit();
        session.close();
    }
}
