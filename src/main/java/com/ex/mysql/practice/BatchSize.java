package com.ex.mysql.practice;

import com.ex.HibernateUtil;
import com.ex.mysql.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class BatchSize {
    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory("mysql");
        batchSize(sessionFactory);
    }


    public static void batchSize(SessionFactory sessionFactory) {
        // mark Set<Authority> authorities; with @BathSize
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        List<User> users = session.createQuery("From User u", User.class).list();
        for(User user : users){
            System.out.println(user.getAuthorities());
        }
        tx.commit();
        session.close();
    }
}
