package com.ex.mysql.practice;

import com.ex.HibernateUtil;
import com.ex.mysql.model.Authority;
import com.ex.postgres.model.Customer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class HibernateState {
    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory("mysql");
//        persist(sessionFactory);
//        deleteDetachEntity(sessionFactory);
//        evict(sessionFactory, 3L);
        load(sessionFactory, 1L);

    }


    public static void load(SessionFactory sessionFactory, Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        System.err.println("Load for first time Session 1: ");
        Authority authority = session.load(Authority.class, id);
        System.err.println(authority);

        System.err.println("Load for second time Session 1: ");
        Authority authority2 = session.load(Authority.class, id);
        System.err.println(authority2);

        tx.commit();
        session.close();


        Session sf2 = sessionFactory.openSession();
        Transaction tx2 = sf2.beginTransaction();

        System.err.println("Load for first time Session 2: ");
        Authority authority3 = sf2.load(Authority.class, id);
        System.err.println(authority3);

        tx2.commit();
        sf2.close();
    }


    // persist a transient object to database (object transform to persistence state)

    /**
     * transient object can to be persistence by call session.save
     *
     * @param sessionFactory
     */
    public static void persist(SessionFactory sessionFactory) {
        Authority authority = new Authority();
        authority.setName("SUPER_ADMIN");
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.merge(authority); // when we invoke session.save -> transient object will be store in persistence context
        tx.commit();
        session.close();
    }


    /**
     * to apply any change object only need to be in persistence context
     * and we does not need to invoke session.merge
     *
     * @param sessionFactory
     */
    public static void update(SessionFactory sessionFactory, Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Authority authority = session.load(Authority.class, id);
        authority.setName("Change without invoke merge");
        tx.commit();
        session.close();
    }


    /**
     * when invoke session.merge , first hibernate will get object from persistence context
     * if found -> return object
     * if not found -> retrieve from database and return new persistence object , so the first object is not in persistence context
     * but the loaded object is in persistence context and can apply any change
     *
     * @param sessionFactory
     */
    public static void mergePitfall(SessionFactory sessionFactory, Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Authority authority = session.get(Authority.class, id);
        tx.commit();
        session.close();
        Session ss2 = sessionFactory.openSession();
        Transaction tx2 = ss2.beginTransaction();
        ss2.merge(authority); // not found in persistence context -> load from database and return new object
        authority.setName("New Name Not Updated"); // because authority not in persistence context
        tx2.commit();
        ss2.close();
    }


    /**
     * remove object does not need object to be in persistence context
     *
     * @param sessionFactory
     */
    public static void deleteTransientEntity(SessionFactory sessionFactory, Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Authority authority = new Authority();
        authority.setId(id);
        session.remove(authority); // object does't need to be in persistence context
        tx.commit();
        session.close();
    }

    /**
     * remove object does not need object to be in persistence context
     *
     * @param sessionFactory
     */
    public static void deleteDetachEntity(SessionFactory sessionFactory, Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Authority authority = session.get(Authority.class, id);
        tx.commit();
        session.close();
        // after this line , authority to be detached object
        Session ss2 = sessionFactory.openSession();
        Transaction tx2 = ss2.beginTransaction();
        ss2.remove(authority); // remove detached entity
        tx2.commit();
        ss2.close();
    }

    /**
     * remove object from persistence context
     * any change to object can not be apply to database
     *
     * @param sessionFactory
     * @param id
     */
    public static void evict(SessionFactory sessionFactory, Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Authority authority = session.get(Authority.class, id);

        session.evict(authority);
        authority.setName("name change"); // detach entity
        tx.commit();
        session.close();
    }

}
