package com.ex.mysql.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "tbl_user")

@NamedEntityGraph(name = "user-entity-graph", attributeNodes = @NamedAttributeNode("authorities"))
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username")
    private String username;


    @OneToMany
    @JoinTable(
            name = "tbl_user_authority",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "authority_id")
    )
//    @BatchSize(size = 2)
    private Set<Authority> authorities;
}
