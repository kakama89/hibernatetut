package com.ex.mysql;

import com.ex.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class Main {
    public static void main(String[] args) {
        SessionFactory sf = HibernateUtil.getSessionFactory("mysql");
        Session s = sf.openSession();
        Transaction tx = s.beginTransaction();

        s.createQuery("From User s").list();
        tx.commit();
    }
}

