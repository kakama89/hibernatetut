package com.ex;

import com.ex.mysql.model.Authority;
import com.ex.mysql.model.User;
import com.ex.postgres.model.Customer;
import com.ex.postgres.model.Partner;
import com.ex.postgres.model.Site;
import net.ttddyy.dsproxy.listener.logging.CommonsLogLevel;
import net.ttddyy.dsproxy.support.ProxyDataSourceBuilder;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataBuilder;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.model.naming.ImplicitNamingStrategyJpaCompliantImpl;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.postgresql.ds.PGSimpleDataSource;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public class HibernateUtil {
    private HibernateUtil() {
    }

    private static Map<String, SessionFactory> factories = new HashMap<>();

    public static SessionFactory getSessionFactory(String databaseType) {
        if ("mysql".equalsIgnoreCase(databaseType)) {
            return build("mysql.conf.xml");
        } else {
            return build("postgres.conf.xml");
        }
    }



    private static SessionFactory build(String url) {
        SessionFactory factory = factories.get(url);
        if (factory == null) {
            Configuration mysql = new Configuration();
            Configuration conf = mysql.configure(url);
            factory = conf.buildSessionFactory();
            factories.put(url, factory);
        }
        return factory;
    }

    public static SessionFactory buildSessionFactoryLogging(String url){
        Map<String, Object> settings = new HashMap<>();
        settings.put(Environment.DATASOURCE, getDataSource(url));
        settings.put(Environment.ORDER_INSERTS, "true");
        settings.put(Environment.STATEMENT_BATCH_SIZE, "3");
        settings.put(Environment.ORDER_UPDATES, "true");
        settings.put(Environment.BATCH_VERSIONED_DATA, "true");
        settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");

        StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
                .applySettings(settings)
                .build();

        // STEP 2: Build Metadata
        MetadataSources metadataSources = new MetadataSources(standardRegistry);
        if ("mysql".equals(url)) {
            metadataSources.addAnnotatedClass(Authority.class)
                    .addAnnotatedClass(User.class);
        } else {
            metadataSources.addAnnotatedClass(Partner.class)
                    .addAnnotatedClass(Site.class)
                    .addAnnotatedClass(Customer.class);
        }

        Metadata metadata = metadataSources.getMetadataBuilder()
                .applyImplicitNamingStrategy(ImplicitNamingStrategyJpaCompliantImpl.INSTANCE)


                .build();
        return metadata.getSessionFactoryBuilder().build();
    }


    private static DataSource getDataSource(String type) {
        PGSimpleDataSource dataSource = new PGSimpleDataSource();
        if ("mysql".equals(type)) {
            dataSource.setUrl("jdbc:mysql://localhost:3306/h_mysql?rewriteBatchedStatements=true");
        } else {
            dataSource.setUrl("jdbc:postgresql://localhost:5432/h_postgresql");
        }
        dataSource.setUser("root");
        dataSource.setPassword("root");
        return ProxyDataSourceBuilder.create(dataSource)
                .logQueryByCommons(CommonsLogLevel.INFO)
                .name("ProxyDataSource")
                .countQuery()
                .multiline()
                .build();
    }
}
