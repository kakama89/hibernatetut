package com.ex.postgres.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "tbl_site")
public class Site {

    @Id
    @SequenceGenerator(name = "site_seq_gen", sequenceName = "site_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "site_seq_gen")
    private Long id;


    @Column
    private String name;

    @Column
    private String alias;

    @ManyToOne
    @JoinColumn
    private Partner partner;

    @OneToMany(mappedBy = "site", cascade = CascadeType.ALL)
    private Set<Customer> customers;
}
