package com.ex.postgres.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name ="tbl_partner")
public class Partner {
    @Id
    @SequenceGenerator(name = "partner_seq_gen", sequenceName = "partner_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "partner_seq_gen")
    private Long id;

    @Column
    private String name;

    @Column
    private String alias;

    @OneToMany(mappedBy = "partner")
    private Set<Site> sites;
}
