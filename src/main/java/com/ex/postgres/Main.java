package com.ex.postgres;

import com.ex.HibernateUtil;
import com.ex.postgres.model.Partner;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class Main {
    public static void main(String[] args) {
        SessionFactory sf = HibernateUtil.getSessionFactory("postgres");
        Session s = sf.openSession();
        Transaction tx = s.beginTransaction();

        s.get(Partner.class, 1L);
        tx.commit();
    }
}
