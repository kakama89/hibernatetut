package com.ex.postgres.practice;

import com.ex.HibernateUtil;
import com.ex.postgres.model.Customer;
import com.ex.postgres.model.Site;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;

public class CascadeStrategy {
    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory("postgres");
//        insert(sessionFactory);
        update(sessionFactory, 1L);
    }

    public static void insertWithoutCascade() {

    }


    // use cascade all with insert
    public static void insert(SessionFactory sessionFactory) {
        Customer customer = new Customer();
        customer.setAlias("C01");

        Site site = new Site();
        site.setName("SN1");
        site.setAlias("SA1");
        site.setCustomers(Arrays.asList(customer).stream().collect(Collectors.toSet()));
        customer.setSite(site);
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.persist(site);
        session.persist(customer);

        tx.commit();
        session.close();
    }

    // use cascade all with delete

    // use cascade all with insert
    public static void update(SessionFactory sessionFactory, Long id) {

        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Site site = session.get(Site.class, id);

        site.setName("Update with cascade224");
//        Set<Customer> customers = site.getCustomers();
//        Iterator<Customer> itr = customers.iterator();
//        while (itr.hasNext()){
//            Customer customer = itr.next();
//            customer.setAlias("changed alias");
//        }
        session.merge(site);
        tx.commit();
        session.close();
    }

    // use cascade all with remove
    public static void delete(SessionFactory sessionFactory, Long id) {

        Session session = sessionFactory.openSession();
        Site site = session.get(Site.class, id);
        Transaction tx = session.beginTransaction();
        session.delete(site);
        tx.commit();
        session.close();
    }
}
