package com.ex.postgres.practice;

import com.ex.HibernateUtil;
import com.ex.postgres.model.Partner;
import com.ex.postgres.model.Site;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;
import java.util.Random;

public class BatchUpdate {
    public static void main(String[] args) {
//        SessionFactory sessionFactory = HibernateUtil.getSessionFactory("postgres");
        SessionFactory sessionFactory = HibernateUtil.buildSessionFactoryLogging("postgres");
        batchInsert(sessionFactory, 3);
//        batchUpdate(sessionFactory, 3);

    }

    public static void batchInsert(SessionFactory sessionFactory, int batchSize) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Random random = new Random();
        for (int i = 1; i <= 10; i++) {
            Partner p = new Partner();
            p.setName("Partner_Name_" + random.nextInt() + "_" + i);
            p.setAlias("Partner_Alias_" + random.nextInt() + "_" + i);
            session.save(p);
            if (i % batchSize == 0) {
                session.flush();
                session.clear();
            }
        }
        tx.commit();
        session.close();

    }

    public static void batchUpdate(SessionFactory sessionFactory, int batchSize) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        List<Site> sites = session.createQuery("FROM Site s", Site.class).list();
        Random random = new Random();
        int i = 0;
        for (Site site : sites) {
            site.setName(site.getName() + "_" + random.nextInt());
            session.merge(site);
            if (i++ % batchSize == 0) {
                session.flush();
                session.clear();
            }
        }
        tx.commit();
        session.close();
    }
}
