-- h_mysql.tbl_authority definition

CREATE TABLE `tbl_authority` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- h_mysql.tbl_user definition

CREATE TABLE `tbl_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- h_mysql.tbl_user_authority definition

CREATE TABLE `tbl_user_authority` (
  `user_id` bigint(20) NOT NULL,
  `authority_id` bigint(20) NOT NULL,
  CONSTRAINT `FK_tbl_user_authority_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `FK_tbl_user_authority_authority_id` FOREIGN KEY (`authority_id`) REFERENCES `tbl_authority` (`id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;