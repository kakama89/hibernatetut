INSERT INTO `tbl_authority` (`id`,`name`) values
(1,'SHIPPER'),
(2,'CUSTOMER'),
(3,'RECEIVER'),
(4,'ADMIN'),
(5,'ACCOUNTANT');

INSERT INTO `tbl_user` (`id`,`username`) values
(1,'toaix1'),
(2,'khoand'),
(3,'kienbn'),
(4,'tungvt'),
(5,'uyennt');

INSERT INTO `tbl_user_authority` (`user_id`,`authority_id`) values
(1,1),
(1,4),
(2,2),
(2,4),
(3,5);