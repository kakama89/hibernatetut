

CREATE SEQUENCE public.partner_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE TABLE public.tbl_partner (
    id bigint NOT NULL DEFAULT nextval('public.partner_id_seq'::regclass),
    alias character varying(255),
    name character varying(255)
);
ALTER SEQUENCE public.partner_id_seq OWNED BY public.tbl_partner.id;

ALTER TABLE ONLY public.tbl_partner
    ADD CONSTRAINT tbl_partner_pkey PRIMARY KEY (id);

CREATE SEQUENCE public.site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.tbl_site (
    id bigint NOT NULL DEFAULT nextval('public.site_id_seq'::regclass),
    alias character varying(255),
    name character varying(255),
    partner_id bigint
);
ALTER SEQUENCE public.site_id_seq OWNED BY public.tbl_site.id;


ALTER TABLE ONLY public.tbl_site
    ADD CONSTRAINT tbl_site_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.tbl_site
    ADD CONSTRAINT fk_tbl_site_partner_id FOREIGN KEY (partner_id) REFERENCES public.tbl_partner(id);




CREATE SEQUENCE public.customer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE TABLE public.tbl_customer (
    id bigint NOT NULL DEFAULT nextval('public.customer_id_seq'::regclass) ,
    alias character varying(255),
    site_id bigint
);
ALTER SEQUENCE public.customer_id_seq OWNED BY public.tbl_customer.id;

ALTER TABLE ONLY public.tbl_customer
    ADD CONSTRAINT tbl_customer_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.tbl_customer
    ADD CONSTRAINT fk_tbl_customer_site_id FOREIGN KEY (site_id) REFERENCES public.tbl_site(id);
