INSERT INTO public.tbl_partner(name,alias) values
('P1','Partner 1'),
('P2','Partner 2'),
('P3','Partner 3'),
('P4','Partner 4'),
('P5','Partner 5');


INSERT INTO public.tbl_site(alias, name, partner_id) values
('SITE_1','Site 01',1),
('SITE_2','Site 02',1),
('SITE_3','Site 03',1),
('SITE_4','Site 04',1),
('SITE_5','Site 05',1),
('SITE_6','Site 06',2),
('SITE_7','Site 07',2),
('SITE_8','Site 08',3),
('SITE_9','Site 09',3),
('SITE_10','Site 10',3);

INSERT INTO public.tbl_customer (alias, site_id) values
('Customer 1',1),
('Customer 2',1),
('Customer 3',2),
('Customer 4',2),
('Customer 5',2);

